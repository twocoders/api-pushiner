package queue

import (
	"fmt"
	"log"

	"../config"
	"github.com/streadway/amqp"
)

func Connection() (*amqp.Connection, *amqp.Channel, amqp.Queue, error) {

	// Load Parameters
	conf := config.LoadConfiguration()
	user, pass, host, port := conf.QueueUser, conf.QueuePassword, conf.QueueHost, conf.QueuePort

	// Connection
	configuration := fmt.Sprint("amqp://", user, ":", pass, "@", host, ":", port, "/")
	conn, err := amqp.Dial(configuration)
	failOnError(err, "Failed to connect to RabbitMQ")
	//    defer conn.Close()

	// Channel
	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	//    defer ch.Close()

	// Queue
	q, err := ch.QueueDeclare(
		"hello", // name
		false,   // durable
		false,   // delete when usused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	failOnError(err, "Failed to declare a queue")

	return conn, ch, q, err
}

func Publish(body string) {

	// Connection
	conn, ch, q, err := Connection()

	// Pubish
	err = ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(string(body)),
		})
	failOnError(err, "Failed to publish a message")

	//Defers
	defer conn.Close()
	defer ch.Close()

}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}
