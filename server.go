package main

import (
	"fmt"
	//    "log"
	"io/ioutil"
	"net/http"

	"./factory/device"
	"./factory/queue"
	"github.com/go-martini/martini"
)

func checkHeader(res http.ResponseWriter, req *http.Request) bool {
	if req.Header.Get("X-API-KEY") != "secret123" {
		res.WriteHeader(http.StatusUnauthorized)
		return true
	}
	return false
}

func main() {
	m := martini.Classic()
	m.Post("/notification/send", func(res http.ResponseWriter, req *http.Request) string {

		// Check headers
		if checkHeader(res, req) {
			return "ko"
		}

		// Request body
		body, err := ioutil.ReadAll(req.Body)
		if err != nil {
			//panic()
		}

		// Publish body data
		queue.Publish(string(body))

		return "Notification received."
	})
	m.Post("/notification/register-device", func(res http.ResponseWriter, req *http.Request) string {
		device.Insert()
		return "Device registered successfully"
	})
	m.Put("/notification/update-device", func(res http.ResponseWriter, req *http.Request) string {
		result := device.FindDevice()
		return fmt.Sprint(result, " Device updated successfully")
	})
	m.Post("/notification/received", func(res http.ResponseWriter, req *http.Request) string {
		return "Notification received successfully"
	})
	m.Run()
}
