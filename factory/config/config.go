package config

import (
	//"encoding/json"
	"fmt"
	//"io/ioutil"
	//"os"
	"github.com/BurntSushi/toml"
	//"log"
)

type Config struct {
	DatabaseHost     string `json:"databaseHost"`
	DatabasePort     string `json:"databasePort"`
	DatabaseName     string `json:"databaseName"`
	DatabaseUser     string `json:"databaseUser"`
	DatabasePassword string `json:"databasePassword"`
	QueueHost        string `json:"queueHost"`
	QueuePort        string `json:"queuePort"`
	QueueUser        string `json:"queueUser"`
	QueuePassword    string `json:"queuePassword"`
}

func LoadConfiguration() Config {

	// Load with toml (hipster++ style)
	var conf Config
	if _, err := toml.DecodeFile("config/parameters.toml", &conf); err != nil {
		// handle error
		fmt.Print("Error:", err)
	}

	/*
		// Read file parameters
		file, e := ioutil.ReadFile("config/parameters.json")
		if e != nil {
			os.Exit(1)
		}

		// Mapped
		var conf Config
		err := json.Unmarshal([]byte(string(file)), &conf)
		if err != nil {
			fmt.Print("Error:", err)
		}
	*/

	return conf
}
