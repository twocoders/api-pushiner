package device

import (
	//"fmt"
	"log"

	"../database"
	"gopkg.in/mgo.v2/bson"
)

type Device struct {
	Application string
	Tags        string
}

func Insert() {

	// Load Connection
	c, err, session := database.Connection()

	// Create MongoCollection
	cl := c.C("Device")

	// Insert Register
	err = cl.Insert(&Device{"App1", "tag1"},
		&Device{"App2", "tag2"})
	if err != nil {
		log.Fatal(err)
	}
	defer session.Close()

}

func FindDevice() Device {

	// Load Connection
	c, err, session := database.Connection()

	// Create MongoCollection
	cl := c.C("Device")

	// Execute Query
	result := Device{}
	err = cl.Find(bson.M{"application": "App1"}).One(&result)
	if err != nil {
		log.Fatal(err)
	}
	defer session.Close()

	return result
}
