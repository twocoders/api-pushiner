# API PUSHINER
[![Build Status](https://drone.io/bitbucket.org/twocoders/api-pushiner/status.png)](https://drone.io/bitbucket.org/twocoders/api-pushiner/latest)


## Install

### Configure gopath

```go
$ mkdir $HOME/go
$ export GOPATH=$HOME/go
$ export PATH=$PATH:$GOPATH/bin
```

### Martini

```go
go get github.com/go-martini/martini
```

### mgo

```go
go get gopkg.in/mgo.v2
```

### amqp

```go
go get github.com/streadway/amqp
```

### toml
```go
github.com/BurntSushi/toml
```

Examples

```
https://github.com/rabbitmq/rabbitmq-tutorials/tree/master/go
```

## Run server

```go
go run server.go
```

## Run consumer

```go
go run worker.go
```
