package database

import (
	//"fmt"
	//"log"

	"../config"
	"gopkg.in/mgo.v2"
	//"gopkg.in/mgo.v2/bson"
)

func Connection() (*mgo.Database, error, *mgo.Session) {

	// Load Parameters
	conf := config.LoadConfiguration()
	host, databaseName := conf.DatabaseHost, conf.DatabasePort

	// Open session in mongo
	session, err := mgo.Dial(host)
	if err != nil {
		panic(err)
	}
	//defer session.Close()

	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)

	// Create mongoDatabase
	c := session.DB(databaseName)

	return c, err, session
}
